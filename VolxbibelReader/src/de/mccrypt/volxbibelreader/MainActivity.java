package de.mccrypt.volxbibelreader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends Activity {
	
	public final static String EXTRA_PASSAGE = "de.mccrypt.VolxbibelReader.PASSAGE";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	public void get_passage(View view) {
		
		//Click on Los Button starts a new View with the Biblepassage
		Intent intent = new Intent(this, NetworkActivity.class);
		EditText input_passage = (EditText) findViewById(R.id.input_bible_passage);
		String passage = input_passage.getText().toString();
		intent.putExtra(EXTRA_PASSAGE, passage);
		startActivity(intent);
		
	}

}
