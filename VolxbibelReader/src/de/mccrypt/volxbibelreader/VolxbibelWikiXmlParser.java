package de.mccrypt.volxbibelreader;

import java.io.IOException;
import java.io.InputStream;
import java.text.Normalizer;
import java.util.regex.Pattern;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Xml;

public class VolxbibelWikiXmlParser {
    // We don't use namespaces
    private static final String ns = null;
    public static final String TAG = "de.mccrypt.volxbibelreader";
   
    public BiblePassage parse(InputStream in) throws XmlPullParserException, IOException {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(in, "UTF-8");
            parser.nextTag();
            return readWikiXml(parser);
        } finally {
            in.close();
        }
    }
    private BiblePassage readWikiXml(XmlPullParser parser) throws XmlPullParserException, IOException {
        //api-call result begins with api TAG
    	parser.require(XmlPullParser.START_TAG, ns, "api");
    	Log.d(TAG, "readWikiXml: found api");
    	String title = null,passage_text=null;
    	
    	while (!(parser.next() == XmlPullParser.END_TAG && parser.getName().equals("api") )) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            // Starts by looking for the entry tag
            if (name.equals("page")) {
            	Log.d(TAG, "readWikiXml: found title");
                title=readTitle(parser);
            } else if(name.equals("rev")) {
            	passage_text=readText(parser);
            	//no information needed after text
            	break;
            }else {
                continue;
            }
        }
        Log.d(TAG, "readWikiXml: read:" + title+" " + passage_text);
        return new BiblePassage(title, passage_text);
    }
    public static class BiblePassage {
        public final String title;
        public final String text;

        private BiblePassage(String title, String bibleTxt) {
            this.title = title;
            this.text = bibleTxt;
        }
        
        @Override
        public String toString() {
        	StringBuilder sb = new StringBuilder();
        	sb.append("<h3>");
        	//Normalizer used because of encoding Issues with (ä,ü,ö etc)
        	sb.append(Normalizer.normalize(title, Normalizer.Form.NFD));
        	sb.append("</h3>");
        	sb.append("<em>");
        	sb.append(Normalizer.normalize(text, Normalizer.Form.NFD));
        	sb.append("</em>");
        	return extractBiblePassage(sb.toString());
        	
        }
    }
     
    // Processes title tags in the feed.
    private String readTitle(XmlPullParser parser) throws IOException, XmlPullParserException {
        //page contains the title
    	parser.require(XmlPullParser.START_TAG, ns, "page");
        String title = parser.getAttributeValue(ns, "title");
        return title;
    }

    // Get Passage Text
    private String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        // rv is the Container of the text
    	parser.require(XmlPullParser.START_TAG, ns, "rev");
    	String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
    
    //Get the url for the specified bible passage
    public static String biblePassageToUrl(String passage) {
		return "http://wiki.volxbibel.com/api.php?format=xml&action=query&prop=revisions&rvprop=content&titles=" + passage.replace(' ', '_');
	}
    
    public static String extractBiblePassage(String txt) {
    	//regex searches for {{...}} and deletes it
    	String regex = "\\Q{{\\E.*\\Q}}\\E";
    	String resultTxt = txt.replaceAll(regex, "");
    	Log.d(TAG, "extractBiblePassage: cutted: "+resultTxt);
    	//reges search for Headlines
    	regex = "===";
    	boolean b = true;

    	while(resultTxt.contains(regex)) {
    		if(b)
    			resultTxt = resultTxt.replaceFirst(regex, "<h4>");
    		else
    			resultTxt = resultTxt.replaceFirst(regex, "</h4>");
    		b = !b;
    		Log.d(TAG, "extractedBibelPassage: Headlines: "+resultTxt);
    	}
    	return resultTxt;
    }
  }
