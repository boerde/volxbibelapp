package de.mccrypt.volxbibelreader;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;

import org.xmlpull.v1.XmlPullParserException;

import de.mccrypt.volxbibelreader.VolxbibelWikiXmlParser.BiblePassage;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

public class NetworkActivity extends Activity {
    public static final String WIFI = "Wi-Fi";
    public static final String ANY = "Any";
    public static final String TAG = "de.mccrypt.volxbibelreader";
    
    private String urlPassage;
   
    // Whether there is a Wi-Fi connection.
    private static boolean wifiConnected = false; 
    // Whether there is a mobile connection.
    private static boolean mobileConnected = true;
    // Whether the display should be refreshed.
    public static boolean refreshDisplay = true; 
    public static String sPref = "Any";

    @Override
    public void onCreate(Bundle savedInstanceState) {
    	
    	super.onCreate(savedInstanceState);
    	
    	String passageName = getIntent().getStringExtra(MainActivity.EXTRA_PASSAGE);
    	
    	urlPassage = VolxbibelWikiXmlParser.biblePassageToUrl(passageName);
    	Log.d(TAG, "Got this URL: " + urlPassage + " to load");
    	loadPage();
    	Log.d(TAG, "NetworkActivity: page loaded");
    	WebView wV_passage = new WebView(this);
    	wV_passage.setId(223);
    	//add Scrollbar
    	wV_passage.setVerticalScrollBarEnabled(true);
    	//IMPORTANT for android 2.3.3 to display "Umlaute"
		wV_passage.getSettings().setDefaultTextEncodingName("utf-8");
    	
		setContentView(wV_passage);
    }
      
    // Uses AsyncTask to download the XML feed from stackoverflow.com.
    public void loadPage() {  
      
        if((sPref.equals(ANY)) && (wifiConnected || mobileConnected)) {
        	Log.d(TAG, "loadPage: started loading...");
            new DownloadXmlTask().execute(urlPassage);
        }
        else if ((sPref.equals(WIFI)) && (wifiConnected)) {
        	Log.d(TAG, "loadPage: started loading...");
            new DownloadXmlTask().execute(urlPassage);
        } else {
            // show error
        	Log.d(TAG, "loadPage:NO CONNECTION");
        }  
    }
    
 // Implementation of AsyncTask used to download XML feed from stackoverflow.com.
    private class DownloadXmlTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {
            try {
            	Log.d(TAG, "DownloadXmlTask: started thread...");
                return loadXmlFromNetwork(urls[0]);
            } catch (IOException e) {
                return "Connection Error";
            } catch (XmlPullParserException e) {
                return "XML-Parser Error";
            }
        }

        @Override
        protected void onPostExecute(String result) {  
            // Displays the HTML string in the UI via a TextView
            WebView text = (WebView) findViewById(223);
            text.loadData(result, "text/html; charset=UTF-8", "UTF-8");
            
        }
    }
    
 // Uploads XML from stackoverflow.com, parses it, and combines it with
 // HTML markup. Returns HTML string.
 private String loadXmlFromNetwork(String urlString) throws XmlPullParserException, IOException {
     InputStream stream = null;
     // Instantiate the parser
     VolxbibelWikiXmlParser volxbibelXmlParser = new VolxbibelWikiXmlParser();
     BiblePassage biblePassage;       
         
     try {
    	 Log.d(TAG, "loadXmlFromNetwork: starting download...");
         stream = downloadUrl(urlString);        
         biblePassage = volxbibelXmlParser.parse(stream);
     // Makes sure that the InputStream is closed after the app is
     // finished using it.
     } finally {
         if (stream != null) {
             stream.close();
         } 
      }
     Log.d(TAG, "Loaded Bible Passage: \""+biblePassage.toString()+"\"" );
     
     return biblePassage.toString();
 }

 // Given a string representation of a URL, sets up a connection and gets
 // an input stream.
 private InputStream downloadUrl(String urlString) throws IOException {
     java.net.URL url = new java.net.URL(urlString);
     HttpURLConnection conn = (HttpURLConnection) url.openConnection();
     conn.setReadTimeout(10000 /* milliseconds */);
     conn.setConnectTimeout(15000 /* milliseconds */);
     conn.setRequestMethod("GET");
     conn.setDoInput(true);
     // Starts the query
     conn.connect();
     return conn.getInputStream();
 }
}
