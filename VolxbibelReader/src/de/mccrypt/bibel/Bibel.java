package de.mccrypt.bibel;

import java.util.TreeSet;

public abstract class Bibel {
	
	public static TreeSet<String> booksOfBibel;
	
	public Bibel() {
	}
	
	public abstract String getPassage(String book, int chapter, int vers);

	
}
